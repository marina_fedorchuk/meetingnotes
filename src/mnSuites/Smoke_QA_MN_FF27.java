package mnSuites;

import java.util.LinkedList;

import junit.framework.JUnit4TestAdapter;
import junit.framework.TestSuite;

import org.junit.runner.RunWith;
import org.junit.runners.AllTests;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;

import setupdata.OrgURLS;
import tests.AccountCreation;
import tests.AddAttachment;
import tests.AddConAccOnRelatedList;
import tests.ContactCreation;
import tests.EventCreation;
import tests.EventEdition;
import tests.OpportunityCreation;
import tests.TaskCreation;
import tests.TaskEdition;
import basefunctionality.TestsBase;
@RunWith(AllTests.class)

public class Smoke_QA_MN_FF27{
	private static WebDriver driver;
	protected static StringBuffer verificationErrors = new StringBuffer(); 
	private static OrgURLS _orgURLS = new OrgURLS();
	
	public static TestSuite suite() throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException
    {
		
		LinkedList<String[]> env = new LinkedList<String[]>();

		env.add(new String[] { Platform.XP.toString(), "firefox", "27", _orgURLS.getOrgQA_MN() });
		
		TestsBase.setEnvironments(env);
		
		TestSuite suite = new TestSuite();
		//suite.addTest(new JUnit4TestAdapter(AccountCreation.class));
		//suite.addTest(new JUnit4TestAdapter(ContactCreation.class));
		//suite.addTest(new JUnit4TestAdapter(OpportunityCreation.class));
		//suite.addTest(new JUnit4TestAdapter(TaskCreation.class));
		//suite.addTest(new JUnit4TestAdapter(TaskEdition.class));
		//suite.addTest(new JUnit4TestAdapter(EventCreation.class));
		//suite.addTest(new JUnit4TestAdapter(EventEdition.class));
		suite.addTest(new JUnit4TestAdapter(AddConAccOnRelatedList.class));
		//suite.addTest(new JUnit4TestAdapter(AddAttachment.class));
		
		
		
	    return suite;
        
     }
	
	
	public static WebDriver getDriver() {
		// TODO Auto-generated method stub
		return driver;
	}

	
}