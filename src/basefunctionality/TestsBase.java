package basefunctionality;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.junit.After;
import org.junit.Before;
import org.junit.runners.Parameterized.Parameters;
import org.openqa.jetty.util.TestCase;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.interactions.MoveTargetOutOfBoundsException;
import org.openqa.selenium.net.UrlChecker.TimeoutException;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Optional;
import com.beust.jcommander.Parameter;
import com.gargoylesoftware.htmlunit.ElementNotFoundException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.apache.commons.io.FileUtils;
import org.apache.xerces.xs.StringList;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.server.DriverFactory;
import setupdata.OrgURLS;

@RunWith(Parallelized.class)
public class TestsBase {
	private String platform;
	private String browserName;
	private String browserVersion;
	private boolean acceptNextAlert = true;
	private static LinkedList<String[]> environments = new LinkedList<String[]>();
	private static LinkedList<String[]> organizations = new LinkedList<String[]>();
	private OrgURLS _calendarOrgsURLS = new OrgURLS();
	
	protected String orgs;
	protected WebDriver driver;
	protected static StringBuffer verificationErrors = new StringBuffer();
	

	@Parameterized.Parameters
	public static LinkedList<String[]> getEnvironments() throws Exception {
		LinkedList<String[]> env = new LinkedList<String[]>();
		//env.add(new String[] { Platform.MAC.toString(), "iPad", "iPad 3rd (7.0)" });
		//env.add(new String[] { Platform.WINDOWS.toString(), "Firefox", "26" });
		//return env;
		return environments;
	}
	
	public static void setEnvironments(LinkedList<String[]> env) {
		environments = env;		
	}
	
	public TestsBase(String platform, String browserName, String browserVersion, String orgs) {
		this.platform = platform;
		this.browserName = browserName;
		this.browserVersion = browserVersion;
		this.orgs = orgs;
	}
	
	@Before
	public void setUp() throws Exception {
		
		DesiredCapabilities capability = new DesiredCapabilities();
		capability.setCapability("platform", platform);
		capability.setCapability("browser", browserName);
		capability.setCapability("browserVersion", browserVersion);
		capability.setCapability("build", "JUnit - Sample");
		capability.setCapability("acceptSslCerts", "true");
		capability.setCapability("browserstack.debug", "true");
		driver = new RemoteWebDriver(
				new URL(
						"http://marina67:wFjnzAm76t5DryNYxrq3@hub.browserstack.com/wd/hub"),
				capability);
		driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
	}
	
	@After
	  public void tearDown() throws Exception {  
	    driver.quit();  
	  }

	protected boolean isElementPresent(By by) {
		try {
			driver.findElement(by);
			return true;
		} catch (NoSuchElementException e) {
			return false;
		}
	}

	protected boolean isAlertPresent() {
		try {
			driver.switchTo().alert();
			return true;
		} catch (NoAlertPresentException e) {
			return false;
		}
	}

	protected String closeAlertAndGetItsText() {
		try {
			Alert alert = driver.switchTo().alert();
			String alertText = alert.getText();
			if (acceptNextAlert) {
				alert.accept();
			} else {
				alert.dismiss();
			}
			return alertText;
		} finally {
			acceptNextAlert = true;
		}
	}

	public WebDriver getDriver(){
		return driver;
	}
	
	protected void customWait() throws InterruptedException{
		new WebDriverWait(driver, 30);
		System.out.println("wait");
	}
	
	protected void gotoTestOrg() throws Exception {
		try{
			driver.get(orgs);
			customWait();
		} catch (NoSuchElementException e){
			System.out.println("Cannot open TestOrg due to exception: " + e.getMessage() + ". The cause of exception is " + e.getCause() +
					". In the next Line of code: " + e.getStackTrace());
			throw new RuntimeException("Cannot open TestOrg");
		}
	}

}
