package tests;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Select;

import basefunctionality.TestsBase;

public class AccountCreation extends TestsBase{
	public AccountCreation(String platform, String browserName,
			String browserVersion, String orgs) {
		super(platform, browserName, browserVersion, orgs);
		// TODO Auto-generated constructor stub
	}

	@Test
	  public void testAccountCreation() throws Exception {
		gotoTestOrg();
		 driver.get("https://na17.salesforce.com/001/o");
		 customWait();
		 driver.findElement(By.name("new")).click();
		 driver.findElement(By.id("acc2")).clear();
		 driver.findElement(By.id("acc2")).sendKeys("M1");
		 driver.findElement(By.id("acc10")).clear();
		 driver.findElement(By.id("acc10")).sendKeys("555");
		 new Select(driver.findElement(By.id("acc6"))).selectByVisibleText("Analyst");
		 new Select(driver.findElement(By.id("acc7"))).selectByVisibleText("Communications");
		 driver.findElement(By.name("save")).click();
	  }
}
