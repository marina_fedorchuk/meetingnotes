package tests;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import basefunctionality.TestsBase;

public class AddConAccOnRelatedList extends TestsBase{

	public AddConAccOnRelatedList(String platform, String browserName,
			String browserVersion, String orgs) {
		super(platform, browserName, browserVersion, orgs);
		// TODO Auto-generated constructor stub
	}
	
	@Test
	  public void testAddCntctAccntOnRltdLst() throws Exception {
		gotoTestOrg();
	    driver.findElement(By.id("phSearchInput")).clear();
	    driver.findElement(By.id("phSearchInput")).sendKeys("Automated Opp");
	    driver.findElement(By.id("phSearchButton")).click();
	    driver.findElement(By.linkText("Automated Opp")).click();
	    driver.switchTo().frame(driver.findElement(By.id("066o0000001TR6e")));
	    driver.findElement(By.linkText("Test Task 1")).click(); 
	    customWait();
	    driver.findElement(By.xpath("(//button[@type='submit'])[2]")).click();
	    WebDriverWait wait = new WebDriverWait(driver,10);
	    wait.until(ExpectedConditions.elementToBeClickable(By.name("typePicklist")));
	    driver.findElement(By.xpath("(//input[@id='relatedListTypeAheadInput'])[2]")).clear();
	    driver.findElement(By.xpath("(//input[@id='relatedListTypeAheadInput'])[2]")).sendKeys("Automated");
	    driver.findElement(By.linkText("Automated Contact")).click();
	    driver.findElement(By.xpath("(//input[@id='relatedListTypeAheadInput'])[3]")).clear();
	    driver.findElement(By.xpath("(//input[@id='relatedListTypeAheadInput'])[3]")).sendKeys("Automated");
	    driver.findElement(By.linkText("Automated Account")).click();
	    driver.findElement(By.xpath("(//button[@type='submit'])[3]")).click();
	    driver.findElement(By.xpath("//button[@type='submit']")).click();
	  }
	

}
