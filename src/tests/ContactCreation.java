package tests;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Select;

import basefunctionality.TestsBase;

public class ContactCreation extends TestsBase{
	public ContactCreation(String platform, String browserName,
			String browserVersion, String orgs) {
		super(platform, browserName, browserVersion, orgs);
		// TODO Auto-generated constructor stub
	}

	@Test
	  public void testContactCreation() throws Exception {
		gotoTestOrg();
		driver.get("https://na17.salesforce.com/003/o");
     	customWait();
	    driver.findElement(By.name("new")).click();
	    driver.findElement(By.id("name_firstcon2")).clear();
	    driver.findElement(By.id("name_firstcon2")).sendKeys("Automated");
	    driver.findElement(By.id("name_lastcon2")).clear();
	    driver.findElement(By.id("name_lastcon2")).sendKeys("Contact");
	    driver.findElement(By.id("con4")).clear();
	    driver.findElement(By.id("con4")).sendKeys("Automated Account");
	    driver.findElement(By.id("con5")).clear();
	    driver.findElement(By.id("con5")).sendKeys("Test Title");
	    driver.findElement(By.id("con10")).clear();
	    driver.findElement(By.id("con10")).sendKeys("333222555");
	    driver.findElement(By.id("con20")).click();
	    driver.findElement(By.id("con20")).clear();
	    driver.findElement(By.id("con20")).sendKeys("the description of the automated contact.");
	    new Select(driver.findElement(By.id("con9"))).selectByVisibleText("Partner");
	    driver.findElement(By.cssSelector("#bottomButtonRow > input[name=\"save\"]")).click();
	  }

}
