package tests;

import org.junit.Test;
import org.openqa.selenium.By;

import basefunctionality.TestsBase;

public class EventEdition extends TestsBase{

	public EventEdition(String platform, String browserName,
			String browserVersion, String orgs) {
		super(platform, browserName, browserVersion, orgs);
		// TODO Auto-generated constructor stub
	}

	@Test
	  public void testEditEvent() throws Exception {
		gotoTestOrg();
	    driver.findElement(By.id("phSearchInput")).click();
	    driver.findElement(By.id("phSearchInput")).clear();
	    driver.findElement(By.id("phSearchInput")).sendKeys("Automated Opp");
	    driver.findElement(By.id("phSearchButton")).click();
	    driver.findElement(By.linkText("Automated Opp")).click();
	    driver.switchTo().frame(driver.findElement(By.id("066o0000001TR6e")));
	    driver.findElement(By.linkText("Test Event1")).click(); 
	    customWait();
	    driver.findElement(By.xpath("(//button[@type='submit'])[2]")).click();
	    driver.findElement(By.name("default")).clear();
	    driver.findElement(By.name("default")).sendKeys("test event desc edited");
	    driver.findElement(By.xpath("(//button[@type='submit'])[3]")).click();
	    driver.findElement(By.xpath("//button[@type='submit']")).click();
	  }
	
}
