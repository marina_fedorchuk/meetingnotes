package tests;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import basefunctionality.TestsBase;

public class AddAttachment extends TestsBase{

	public AddAttachment(String platform, String browserName,
			String browserVersion, String orgs) {
		super(platform, browserName, browserVersion, orgs);
		// TODO Auto-generated constructor stub
	}
	
	 @Test
	  public void testAddAtchmnt() throws Exception {
		 gotoTestOrg();
		driver.findElement(By.id("phSearchInput")).sendKeys("Automated Opp");
		driver.findElement(By.id("phSearchButton")).click();
		driver.findElement(By.linkText("Automated Opp")).click();
		driver.switchTo().frame(driver.findElement(By.id("066o0000001TR6e")));
		driver.findElement(By.linkText("Test Task 1")).click(); 
		customWait();
		driver.findElement(By.xpath("(//button[@type='submit'])[2]")).click();
		WebDriverWait wait = new WebDriverWait(driver,10);
		customWait();
		wait.until(ExpectedConditions.elementToBeClickable(By.name("typePicklist")));
		customWait();
		driver.findElement(By.id("textInputDefault")).click();
	    driver.findElement(By.name("default")).clear();
	    driver.findElement(By.name("default")).sendKeys("test task desc1 edited");
	    customWait();
		//driver.switchTo().window("Attachments");
		customWait();
		WebElement element = driver.findElement(By.id("gbqfd"));
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", element);
		
		driver.findElement(By.xpath("//html/body/form/div[1]/div[3]/input")).click();
		//wait.until(ExpectedConditions.elementToBeClickable(By.id("//input[contains(@id,'fileAttachment')]")));
		//driver.findElement(By.xpath("//*[@id='j_id0:j_id5:fileAttachment']")).click();
		/*driver.findElement(By.id("//*[@id='j_id0:j_id5:fileAttachment']")).click();
		WebElement fileInput = driver.findElement(By.xpath("//input[@type='file']"));
		fileInput.sendKeys("C:\\Users\\mfedorchuk\\Desktop\\Automation+CI.jpg");
	    driver.findElement(By.xpath("(//button[@type='submit'])[3]")).click();
	    driver.findElement(By.xpath("//button[@type='submit']")).click();*/
	  }

}
