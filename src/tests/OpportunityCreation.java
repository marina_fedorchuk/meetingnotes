package tests;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Select;

import basefunctionality.TestsBase;

public class OpportunityCreation extends TestsBase{
	 public OpportunityCreation(String platform, String browserName,
			String browserVersion, String orgs) {
		super(platform, browserName, browserVersion, orgs);
		// TODO Auto-generated constructor stub
	}

	@Test
	  public void testCreateOpp() throws Exception {
		gotoTestOrg();
		driver.get("https://na17.salesforce.com/006/o");
		customWait();
	    driver.findElement(By.name("new")).click();
	    driver.findElement(By.id("opp3")).click();
	    driver.findElement(By.id("opp3")).clear();
	    driver.findElement(By.id("opp3")).sendKeys("Automated Opp");
	    driver.findElement(By.id("opp4")).clear();
	    driver.findElement(By.id("opp4")).sendKeys("Automated Account");
	    new Select(driver.findElement(By.id("opp5"))).selectByVisibleText("Existing Business");
	    new Select(driver.findElement(By.id("opp6"))).selectByVisibleText("Seminar - Internal");
	   driver.findElement(By.id("opp9")).clear();
	    driver.findElement(By.id("opp9")).sendKeys("11/3/2014");
	    new Select(driver.findElement(By.id("opp11"))).selectByVisibleText("Value Proposition");
	    driver.findElement(By.id("opp9")).click();
	    driver.findElement(By.id("opp9")).clear();
	    driver.findElement(By.id("opp9")).sendKeys("11/3/2016");
	    driver.findElement(By.id("topButtonRow")).click();
	    driver.findElement(By.id("opp14")).click();
	    driver.findElement(By.id("opp14")).clear();
	    driver.findElement(By.id("opp14")).sendKeys("automated opportunity for testing");
	    driver.findElement(By.cssSelector("#bottomButtonRow > input[name=\"save\"]")).click();
	  }

}
