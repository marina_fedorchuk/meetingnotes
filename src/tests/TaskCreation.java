package tests;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import basefunctionality.TestsBase;

public class TaskCreation extends TestsBase{
	public TaskCreation(String platform, String browserName,
			String browserVersion, String orgs) {
		super(platform, browserName, browserVersion, orgs);
		// TODO Auto-generated constructor stub
	}

	@Test
	  public void testCreateTask() throws Exception {
		gotoTestOrg();
	    driver.findElement(By.id("phSearchInput")).clear();
	    driver.findElement(By.id("phSearchInput")).sendKeys("Automated Opp");
	    driver.findElement(By.id("phSearchButton")).click();
	    driver.findElement(By.linkText("Automated Opp")).click();
	    customWait();
	    customWait();
	    driver.switchTo().frame(driver.findElement(By.id("066o0000001TR6e")));
	    driver.findElement(By.name("Task")).click();
	    customWait();
	    customWait();
	    customWait();
	    customWait();
	    customWait();
	    driver.findElement(By.id("textInputDefault")).clear();
	    driver.findElement(By.id("textInputDefault")).sendKeys("Test Task 1");
	    driver.findElement(By.name("typePicklist")).click();
	    driver.findElement(By.linkText("Other")).click();
	    customWait();
	    driver.findElement(By.name("default")).clear();
	    driver.findElement(By.name("default")).sendKeys("test task desc1");
	    driver.findElement(By.xpath("(//button[@type='submit'])[3]")).click();
	    WebDriverWait wait = new WebDriverWait(driver,10);
	    wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt("066o0000001TR6e"));
	    wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("Test Task 1")));
	    customWait();
	  }
}
