package tests;

import org.junit.Test;
import org.openqa.selenium.By;

import basefunctionality.TestsBase;

public class EventCreation extends TestsBase{

	public EventCreation(String platform, String browserName,
			String browserVersion, String orgs) {
		super(platform, browserName, browserVersion, orgs);
		// TODO Auto-generated constructor stub
	}

	 @Test
	  public void testCreateEvent() throws Exception {
		 gotoTestOrg();
	    driver.findElement(By.id("phSearchInput")).clear();
	    driver.findElement(By.id("phSearchInput")).sendKeys("Automated Opp");
	    driver.findElement(By.id("phSearchButton")).click();
	    driver.findElement(By.linkText("Automated Opp")).click();
	    driver.switchTo().frame(driver.findElement(By.id("066o0000001TR6e")));
	    driver.findElement(By.name("Event")).click();
	    driver.findElement(By.id("textInputDefault")).clear();
	    driver.findElement(By.id("textInputDefault")).sendKeys("Test Event1");
	    driver.findElement(By.name("typePicklist")).click();
	    driver.findElement(By.linkText("Other")).click();
	    driver.findElement(By.xpath("//form[@id='detailSection']/div[4]/span/div/div/div/div/div/input")).click();
	    driver.findElement(By.xpath("(//button[@type='button'])[18]")).click();
	    driver.findElement(By.name("default")).click();
	    driver.findElement(By.name("default")).clear();
	    driver.findElement(By.name("default")).sendKeys("test event desc");
	    driver.findElement(By.xpath("(//button[@type='submit'])[3]")).click();
	  }
	
}
